package sample;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class Controller {
    @FXML
    private AnchorPane settings_pane;
    @FXML
    private ImageView btn_settings;
    @FXML
    private ImageView btn_close;
    @FXML
    private ImageView btn_close_settings;
    @FXML
    private void handleButtonAction(MouseEvent event){
        if(event.getSource() == btn_close){
            System.exit(0);
        }else if(event.getSource() == btn_settings){
            settings_pane.setVisible(true);
        }
        if(event.getSource() == btn_close_settings){
            settings_pane.setVisible(false);
        }
    }

}
